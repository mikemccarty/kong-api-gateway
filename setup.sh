# If you want to use the ZD Postgres server, create the kong user and db.

# Uncomment to run dedicated postgres container.
#docker run -d --name kong-database \
#              -p 5432:5432 \
#              -e "POSTGRES_USER=kong" \
#              -e "POSTGRES_DB=kong" \
#              postgres:9.4

docker run -d --name kong \
              --link postgres-zd2.4:kong-database \
              -e "KONG_DATABASE=postgres" \
              -e "KONG_PG_HOST=kong-database" \
              -p 8000:8000 \
              -p 8443:8443 \
              -p 8001:8001 \
              -p 7946:7946 \
              -p 7946:7946/udp \
              kong
