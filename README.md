# README #

This repo holds a brief POC for using Kong as an API Gateway for Zoomdata Internal and Public APIs.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Setup ###

```
./setup.sh
./configure.sh <IP Address for host running server>
```

### Test ###

```
curl --user "admin:secret" http://localhost:8000/zoomdata/service/sources --header 'Host: zoomdata.com' | python -m json.tool
```
