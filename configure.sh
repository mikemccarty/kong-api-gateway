curl http://127.0.0.1:8001 | python -m json.tool

curl -i -X DELETE   --url http://localhost:8001/apis/zoomdata

curl -i -X POST \
  --url http://localhost:8001/apis/ \
  --data 'name=zoomdata' \
  --data "upstream_url=http://$1:8080/" \
  --data 'request_host=zoomdata.com'

curl -X POST http://localhost:8001/apis/zoomdata/plugins --data "name=basic-auth" --data "config.hide_credentials=true"

curl -d "username=admin&custom_id=zoomdata" http://localhost:8001/consumers/

curl -X POST http://localhost:8001/consumers/admin/basic-auth --data "username=admin" --data "password=secret"
